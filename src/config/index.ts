import { TypeOrmModuleOptions } from "@nestjs/typeorm"

const config = {
  db: {
    type: "postgres",
    host: "localhost",
    port: 5432,
    username: "postgres",
    password: "password",
    database: "postgres",
    autoLoadEntities: true,
    keepConnectionAlive: true,
    synchronize: true,
    cache: {
      // type: "database",
      // type: "redis",
      type: "ioredis",
      host: "localhost",
      port: 6379,
      duration: 1_000_000, // in ms by default 1000
    },
  } as TypeOrmModuleOptions,
}

export type TConfig = typeof config

export default (): TConfig => config
