import { Body, Controller, Delete, Get, Inject, Param, Post, Put } from "@nestjs/common"
import { AppService } from "./app.service"
import { PostEntity } from "./entities"

@Controller()
export class AppController {
  @Inject() private readonly appService: AppService

  @Get()
  public async list(): Promise<PostEntity[]> {
    return this.appService.list()
  }

  @Get(":id")
  public async findOne(@Param("id") id: string): Promise<PostEntity> {
    return this.appService.findOne(id)
  }

  @Post()
  public async create(@Body() data: PostEntity): Promise<PostEntity> {
    return this.appService.create(data)
  }

  @Put(":id")
  public async update(@Param("id") id: string, @Body() data: PostEntity): Promise<PostEntity> {
    return this.appService.update(id, data)
  }

  @Delete(":id")
  public async remove(@Param("id") id: string): Promise<void> {
    await this.appService.remove(id)
  }
}
