import { Module } from "@nestjs/common"
import { ConfigModule, ConfigService } from "@nestjs/config"
import { TypeOrmModule, TypeOrmModuleAsyncOptions } from "@nestjs/typeorm"
import { AppController } from "./app.controller"
import { AppService } from "./app.service"
import config, { TConfig } from "./config"
import { PostEntity } from "./entities"

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [config],
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService<TConfig>) =>
        configService.get<TypeOrmModuleAsyncOptions>("db"),
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([PostEntity]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
