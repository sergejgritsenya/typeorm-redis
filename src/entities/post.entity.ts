import { ApiProperty } from "@nestjs/swagger"
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm"

@Entity("post")
export class PostEntity {
  @PrimaryGeneratedColumn("uuid")
  public id: string

  @ApiProperty({ example: "test" })
  @Column()
  public name: string

  @ApiProperty({ example: "test" })
  @Column()
  public text: string

  @CreateDateColumn()
  public created: Date

  @UpdateDateColumn()
  public updated: Date

  @DeleteDateColumn()
  public deleted: Date
}
