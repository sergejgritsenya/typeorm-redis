import { Inject, Injectable } from "@nestjs/common"
import { ConfigService } from "@nestjs/config"
import { InjectDataSource, InjectRepository } from "@nestjs/typeorm"
import { DataSource, Repository } from "typeorm"
import { QueryResultCache } from "typeorm/cache/QueryResultCache"
import { PostEntity } from "./entities"

@Injectable()
export class AppService {
  private readonly queryResultCache: QueryResultCache

  @Inject() private readonly configService: ConfigService
  @InjectRepository(PostEntity) private readonly repository: Repository<PostEntity>

  constructor(@InjectDataSource() dataSource: DataSource) {
    this.queryResultCache = dataSource.queryResultCache
  }

  public async list(): Promise<PostEntity[]> {
    return this.repository.find({
      cache: {
        id: PostEntity.name,
        milliseconds: this.configService.get("db.cache.duration"),
      },
    })
  }

  public async findOne(id: string): Promise<PostEntity> {
    const post = await this.repository.findOne({
      where: {
        id,
      },
      cache: {
        id: `${PostEntity.name}:${id}`,
        milliseconds: this.configService.get("db.cache.duration"),
      },
    })

    return post
  }

  public async create(input: Partial<PostEntity>): Promise<PostEntity> {
    const post = await this.repository.save(input)
    await this.queryResultCache.remove([PostEntity.name, `${PostEntity.name}:${post.id}`])

    return post
  }

  public async update(id: string, input: Partial<PostEntity>): Promise<PostEntity> {
    const post = await this.repository.save({ ...input, id })
    await this.queryResultCache.remove([PostEntity.name, `${PostEntity.name}:${id}`])

    return post
  }

  public async remove(id: string): Promise<void> {
    await this.repository.softDelete(id)
    await this.queryResultCache.remove([PostEntity.name, `${PostEntity.name}:${id}`])
  }
}
